<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <!--Compatibilità con Microsoft e Responsività-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>XRF analysis viewer</title>

    <!-- CSS -->
      <link href="src/css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" type="text/css" href="style.css">
      <link rel="stylesheet" type="text/css" href="src/bootstrap-treeview/dist/bootstrap-treeview.min.css">
      <link rel="stylesheet" type="text/css" href="src/bootstrap-select/dist/bootstrap-select.min.css">
      <link rel="stylesheet" type="text/css" href="src/font-awesome/dist/font-awesome.min.css">

  </head>

  <body>
    <!-- Spinner -->
    <div align="center">
      <div  id="load-spinner" style="display:none">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <!-- /.spinner -->

    <!-- Navbar -->
    <nav class="navbar navbar-fixed-top bg-primary">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="https://oc-chnet.cr.cnaf.infn.it:8443/Laura/XRF-App/">
            <img id="logo-chnet" src="Logo-chnet.png" width="35" height="35" alt="" class="img-rounded">
            <div id="testo-logo-chnet">XRF Analysis Viewer</div>
          </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <?php
                $pic=$_SERVER["HTTP_OIDC_CLAIM_PICTURE"];
                if (filter_var($pic, FILTER_VALIDATE_URL))
                {
                  echo "<img src=\"$pic\" class=\"img-circle\" width=\"25\">";
                } else {
                  echo "<img src=\"./default-avatar.png\" class=\"img-circle\" width=\"25\">";
                }
                // echo "    ".ucfirst($_SERVER["HTTP_OIDC_CLAIM_NAME"]);
              ?>
              <span class="glyphicon glyphicon-chevron-down"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <?php
                  echo "<div class=\"usrname\">".ucfirst($_SERVER["HTTP_OIDC_CLAIM_NAME"])."</div>";
                  echo "<i class=\"usrmail\">".ucfirst($_SERVER["HTTP_OIDC_CLAIM_EMAIL"])."</i>";
                ?>
              </li>
              <li class="divider"></li>
              <li class="usr-menu"><a href="https://chnet-iam.cloud.cnaf.infn.it/dashboard#/home">Profile</a></li>
              <li class="usr-menu"><a href="/Laura/.redirect/?logout=http%3A%2F%2Fchnet.infn.it%2F">Sign out</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
    <!-- ./navbar -->

    <!-- Sidenav -->
    <div class="w3-bar-block w3-border">
      <div class="w3-bar-item w3-button">
        <a href="https://oc-chnet.cr.cnaf.infn.it:8443/Laura/XRF-App/">
          <i class="fa fa-home" data-toggle="tooltip" data-placement="right" title="Home"></i>
          <span class="text-sidenav" style="display:none">Home</span>
        </a>
      </div>
      <div class="w3-bar-item w3-button" data-toggle="modal" data-target="#myModal">
        <i class="fa fa-folder-o" data-toggle="tooltip" data-placement="right" title="Open File"></i>
        <span class="text-sidenav" style="display:none">Open file</span>
      </div>
      <div class="w3-bar-item w3-button" data-toggle="collapse" data-target=".collapse-settings">
        <i class="fa fa-cog" data-toggle="tooltip" data-placement="right" title="Settings"></i>
        <span class="text-sidenav" style="display:none">Settings</span>
      </div>
      <div class="w3-bar-item w3-button" id="reset">
        <i class="fa fa-refresh" data-toggle="tooltip" data-placement="right" title="Reset"></i>
        <span class="text-sidenav" style="display:none">Refresh</span>
      </div>
      <div class="w3-bar-item w3-button">
        <a id="ExportLink" download="Canvas.jpg" href="#" class="w3-bar-item w3-button">
          <div id="ExportImage">
            <i class="fa fa-file-image-o" data-toggle="tooltip" data-placement="right" title="Export Map"></i>
            <span class="text-sidenav" style="display:none">Export Map</span>
          </div>
        </a>
      </div>
      <div class="w3-bar-item w3-button">
        <a href="#" id="ExportGraph" download="Spectrum.jpg" class="w3-bar-item w3-button">
          <i class="fa fa-bar-chart" data-toggle="tooltip" data-placement="right" title="Export Chart"></i>
          <span class="text-sidenav" style="display:none">Export Chart</span>
        </a>
      </div>
      <div class="w3-bar-item w3-button collapse-sidebar-manually sidebar-label">
        <i id="collapse-symbol" class="fa fa-angle-double-right" data-toggle="tooltip" data-placement="right" title="Open Sidebar"></i>
        <span class="text-sidenav" style="display:none">Collapse sidebar</span>
      </div>
    </div>
    <!-- ./sidenav -->

    <!-- Modal -->
    <div id="myModal" class="modal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title">Import File From Server</h3>
          </div>
          <div class="modal-body">
            <div id="FileTreeview"></div>
            <h3> Import File From Local Repository </h3>
            <label class="btn-bs-file btn btn-primary">
              <span class="glyphicon glyphicon-open"></span> Choose a file...
              <input id="myImport" type="file" / >
            </label>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    <!-- ./modal -->

    <!-- COLLAPSABLE PAGE -->
    <div class="container-fluid droppable" id="myContent" dropzone="copy f:text/plain">

      <!-- PAGE CONTENT -->
      <div class="row">

        <!-- MAPPA -->
        <div class="col-sm-4">
          <div class="well" id="mappa-pannel">
            <h2>XRF Image</h2>

            <!-- canvas -->
            <div style="position: relative; height=400px;">
              <canvas id="selectionCanvas" height="400px" width="360px" onmousedown="findPosDown(event);" onmouseup="findPosUp(event);"></canvas>
              <canvas id="myCanvas" height="400px" width="360px"></canvas>
            </div>
            <!-- ./canvas -->

            <div class="collapse collapse-settings" align="center">
              <div class="panel panel-default">
                <div class="panel-body">
                  Saturation<input type="range" id="SaturationSlider" value="100">
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-body">
                  Opacity<input type="range" id="TrasparencySlider" value="0">
                </div>
              </div>
              <button type="button" class="btn btn-primary btn-sm" id="rePlot">Re-color</button>
            </div>

          </div>
          <!-- ./ pannello-mappa -->

        </div>
        <!-- ./mappa -->

        <!-- SPETTRO -->
        <div class="col-md-8">
          <div class="well" id="chart-pannel">

            <h2>XRF Spectrum</h2>

            <!-- grafico -->
            <p id="chart" class="p-chart"></p>
            <img id="chartToImg" style="display:none;">

            <!-- impostazioni -->
            <p><div class="collapse collapse-settings" align="center">
              
              <!-- Asse y -->
              <div class="btn-group">
                <button type="button" class="btn dropdown-toggle btn-primary btn-sm" data-toggle="dropdown">Scale <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#" id="setlinearButton">Linear</a></li>
                  <li><a href="#" id="setlogButton">Log</a></li>
                </ul>
              </div>
              
              <!-- Asse x -->
              <div class="btn-group">
                <button type="button" class="btn dropdown-toggle btn-primary btn-sm" data-toggle="dropdown">x Label <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#" id="setEnergyButton">Energy</a></li>
                  <li><a href="#" id="setChannelsButton">Channels</a></li>
                </ul>
              </div>

              <!-- Selezione picco -->
              <div class="btn-group">
                <div class="form-group" id="myform">
                  <label for="spinBoxMin"><h6>Peak selection (only calibrated)</h6></label>
                  <p><input type="number" class="form-control" id="spinBoxMin" min="0" max="55" step="0.01">
                  <input type="number" class="form-control" id="spinBoxMax" min="0" max="55" step="0.01"></p>
                  <div align="center">
                    <button id="readSpinbox" class="btn btn-primary btn-sm" style="margin-left: -25px;">Select range</button>
                  </div>
                </div>
              </div>

              <!-- Selezione elemento -->
              <select class="form-control selectpicker" data-style="btn-primary btn-sm" data-width="25%" data-live-search="true" id="elementSelect">
                <option value="0">- inspect element -</option>
                <option value="1">Ca</option>
                <option value="2">Pb</option>
                <option value="3">Hg</option>
                <option value="4">Fe</option>
                <option value="5">Cu</option>
                <option value="6">Zn</option>
                <option value="7">Ti</option>
                <option value="8">K</option>
                <option value="9">Co</option>
              </select>

            </div></p>
            <!-- ./impostazioni -->

          </div>
          <!-- ./pannello-spettro -->

        </div>
        <!-- ./colonna-spettro -->

      </div>
      <!-- ./riga 1 -->
    </div>
    <!-- /.collapsable-page -->

    <!-- Librerie -->
      <script type="text/javascript" src="src/jquery/dist/jquery.min.js"></script>
      <script type="text/javascript" src="src/js/bootstrap.min.js"></script>      
      <script type="text/javascript" src="src/bootstrap-treeview/dist/bootstrap-treeview.min.js"></script>
      <script type="text/javascript" src="src/bootstrap-select/dist/bootstrap-select.min.js"></script>
      <script type="text/javascript" src="loaddir.js"></script>
      <script type="text/javascript" src="start.js"></script>
      <script type="text/javascript" src="src/dygraph/dist/dygraph-combined-dev.js"></script>
      <script type="text/javascript" src="src/dygraph/dist/dygraph-extra.js"></script>
    <!-- ./Librerie -->  
  </body>

</html>
