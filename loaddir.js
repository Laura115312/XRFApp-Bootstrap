//funzione che definisce tutti gli elementi responsabili dell'apertura di un file.
//Sono definiti quindi l'albero e il bottone per l'importazione da locale
function setImportFile() {
    //genero e leggo il contenuto della directory "filesystem"
    var xmlDoc;
    var xmlListingFile = new XMLHttpRequest();
    xmlListingFile.open("PROPFIND", "https://oc-chnet.cr.cnaf.infn.it:8443/Laura/XRF-App/XRF-mappe/", false);
    xmlListingFile.setRequestHeader("Depth", "infinity");
    xmlListingFile.onreadystatechange = function () {
        if (xmlListingFile.readyState === 4) {
            if (xmlListingFile.status === 207) {
                var parser = new DOMParser();
                xmlDoc = parser.parseFromString(xmlListingFile.responseText, "text/xml");
            }
        }
    };
    xmlListingFile.send(null);
    //ora genero l'albero e definisco l'evento in caso di selezione di un nodo
    $('#FileTreeview').treeview({ data: generateTree(xmlDoc) });
    $('#FileTreeview').on('nodeSelected', function (e, node) {
        if (node['url'] != undefined) {
            $("#load-spinner").css("display", "inline");
            openFileFromServer(node['url']);
        }
    });
    //bottone per l'importazione locale
    var fileInputButton = document.getElementById('myImport');
    fileInputButton.onchange = function () {
        $("#load-spinner").css("display", "inline");
        var fileName = fileInputButton.files[0];
        var readerObject = new FileReader();
        readerObject.readAsBinaryString(fileName);
        readerObject.onload = function () {
            var fileString = readerObject.result;
            readData(fileString);
        };
    };
}
//funzione che genera automaticamente l'albero
function generateTree(xmlDoc) {
    var tree = [];
    var first = true;
    var oldFolderParent = "";
    var oldFolder = "";
    //inizio leggendo tutti gli elementi da inserire nell'albero (caratterizzati
    //dall'avere un url di riferimento)
    var entry = xmlDoc.getElementsByTagName("D:href");
    //per ogni elemento controllo se si tratta di una cartella o di un documento
    for (var i = 0; i < entry.length; i++) {
        var path = entry[i].childNodes[0].nodeValue.split("");
        //cartella, creo l'oggetto corrsipondente
        if (path[path.length - 1] == "/") {
            var folderName = entry[i].childNodes[0].nodeValue.split("/");
            var Folder = {
                text: folderName[folderName.length - 2],
                nodes: []
            };
            //posiziono la radice del file system, ne memorizzo il path e il padre
            if (first) {
                tree.push(Folder);
                first = false;
                oldFolder = entry[i].childNodes[0].nodeValue;
                oldFolderParent =
                    oldFolder.slice(0, oldFolder.lastIndexOf(folderName[folderName.length - 2]));
            }
            else {
                var newFolder = entry[i].childNodes[0].nodeValue;
                var newFolderParent = newFolder.slice(0, newFolder.lastIndexOf(folderName[folderName.length - 2]));
                //cartella sorella con quella memorizzata
                if (newFolderParent == oldFolderParent) {
                    oldFolder = newFolder;
                    insertOBJinFS(Folder, tree, folderName, 0);
                }
                else if (newFolderParent == oldFolder) {
                    oldFolder = newFolder;
                    oldFolderParent = newFolderParent;
                    insertOBJinFS(Folder, tree, folderName, 0);
                }
                else {
                    //arretro nell'albero fino a trovare lo stesso padre. Per fare questo
                    //tolgo al padre memorizzato in precedenza prima "/" poi il nome dell'
                    //ultima cartella
                    while (newFolderParent != oldFolderParent) {
                        oldFolderParent = oldFolderParent.slice(0, oldFolderParent.length - 1);
                        oldFolderParent = oldFolderParent.slice(0, (oldFolderParent.lastIndexOf("/") + 1));
                    }
                    oldFolder = newFolder;
                    insertOBJinFS(Folder, tree, folderName, 0);
                }
            }
        }
        else {
            var fileName = entry[i].childNodes[0].nodeValue.split("/");
            var filePath = "https://oc-chnet.cr.cnaf.infn.it:8443"
                + entry[i].childNodes[0].nodeValue;
            var File = {
                text: fileName[fileName.length - 1],
                icon: "glyphicon glyphicon-file",
                selectedIcon: "glyphicon glyphicon-file",
                url: filePath
            };
            insertOBJinFS(File, tree, fileName, 1);
        }
    }
    return tree;
}
//funzione che posiziona l'oggetto passato in input nell'albero
function insertOBJinFS(objfs, tree, objfsName, type) {
    //determino la profondità dell'oggetto (se è un file devo aggiungere 1 a causa di "/")
    var depth = objfsName.length;
    if (type)
        depth++;
    //in base alla profondità determino a quale oggetto agganciare quello in input
    var treePosition;
    var l = tree.length - 1;
    switch (depth) {
        case 6:
            treePosition = tree;
            break;
        case 7:
            treePosition = tree[l].nodes;
            break;
        case 8:
            treePosition =
                tree[l].nodes[tree[l].nodes.length - 1].nodes;
            break;
        case 9:
            treePosition =
                tree[l].nodes[tree[l].nodes.length - 1].nodes[tree[l].nodes[tree[l].nodes.length - 1].nodes.length - 1].nodes;
            break;
        case 10:
            treePosition =
                tree[l].nodes[tree[l].nodes.length - 1].nodes[tree[l].nodes[tree[l].nodes.length - 1].nodes.length - 1].nodes[tree[l].nodes[tree[l].nodes.length - 1].nodes[tree[l].nodes[tree[l].nodes.length - 1].nodes.length - 1].nodes.length - 1].nodes;
            break;
        default: break;
    }
    treePosition[treePosition.length - 1].nodes.push(objfs);
}
//funzione che dato l'url di un file, lo apre e lo legge passandone il contenuto
//alla funzione readData(). Questa funzione è invocata quando viene selezionato
//un file dall'albero
function openFileFromServer(url) {
    var fileName = url.split("/");
    console.log("Try to open " + fileName[fileName.length - 1] + " ...");
    var txtFile = new XMLHttpRequest();
    txtFile.open("GET", url, true);
    txtFile.onreadystatechange = function () {
        if (txtFile.readyState === 4) {
            if (txtFile.status === 200) {
                readData((txtFile.responseText));
            }
        }
    };
    txtFile.send(null);
}
